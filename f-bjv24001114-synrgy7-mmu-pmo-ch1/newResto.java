import java.util.LinkedHashMap;
import java.util.Map;
import java.util.HashMap;
import java.util.Scanner;

public class newResto {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        Map<String, Integer> menuPrices = new LinkedHashMap<>() {
            {
                put("Sate Padang", 20000);
                put("Rendang Sapi", 30000);
                put("Nasi Garem", 4000);
                put("Es Teh", 3000);
                put("Es Jeruk", 5000);
            }
        };

        Map<String, Integer> pesanan = new HashMap<>();
        Map<String, Integer> hargaPerItem = new HashMap<>();

        boolean selesai = false;

        do {
            System.out.println("\n==========================");
            System.out.println("Selamat datang di BinarFud");
            System.out.println("==========================");
            System.out.println("\nSilahkan pilih makanan:");
            int menuNumber = 1;
            for (Map.Entry<String, Integer> entry : menuPrices.entrySet()) {
                String menu = entry.getKey();
                int price = entry.getValue();
                System.out.println(menuNumber + ". " + menu + " - Rp" + price);
                menuNumber++;
            }
            System.out.println("99. Pesan dan Bayar");
            System.out.println("0. Keluar aplikasi");

            System.out.println("\n=> ");

            int selectedMenu = input.nextInt();

            if (selected >= 1 && selectedMenu <= 5) {
                String menu = "";
                int price = 0;

                int i = 1;
                for (Map.Entry<String, Integer> entry : menuPrices.entrySet()) {
                    if(i == selectedMenu) {
                        menu = entry.getKey();
                        price = entry.getValue();
                        break;
                    }
                    i++;
                }

                if (pesanan.containsKey(menu)) {
                    pesanan.remove(menu);
                    hargaPerItem.remove(menu);
                }

                System.out.println("\n==========================");
                System.out.println("Berapa pesanan anda");
                System.out.println("==========================");
                int counter = 1;
                for (Map.Entry<String, Integer> entry : menuPrices.entrySet()) {
                    if (counter == selectedMenu) {
                        System.out.println("\n" + entry.getKey() + " - Rp" + entry.getValue());
                    }
                }
                System.out.println("(input 0 untuk kembali)");
                System.out.print("\nqty => ");
                int qty = input.nextInt();

                if (qty == 0) {
                    System.out.println("Pilihan dibatalkan, kembali ke list menu.");
                } else if (qty > 0) {
                    pesanan.put(menu, qty);
                    hargaPerItem.put(menu, price);

                    System.out.println("Pesanan " + menu + " sebanyak " + qty + " porsi telah ditambahkan." );
                }
            } else if (selectedMenu == 99) {
                displayConfirmation(pesanan, hargaPerItem, input);
            } else if (selectedMenu == 0) {
                selesai = true;
            } else {
                System.out.println("Menu tidak tersedia.");
            }
        } while (!selesai);

        input.close();
    }


}